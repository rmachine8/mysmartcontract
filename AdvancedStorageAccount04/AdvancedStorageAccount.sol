// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;
//Create contract with array
contract AdvancedStorage {
    //integer with unit type
    uint[] public ids;

    //Add new value to the array (careful, started from 0 like in JS)
    function add (uint id) public {
        ids.push(id);
    }
    //Get exactly possition of array 
    function get (uint position) view public returns (uint){
        return ids[position];
    }
    //To list all values of array
    function getAll() view public returns (uint [] memory) {
        return ids;
    }
    //Function to get lenght of array 
    function lenght() view public returns (uint) {
        return ids.length;
    }
}


// Gas limit - when send transaction to the etherium network you have to pay "gas". 
// Gas is an abstract unit that measure the computational diffulty of executing the transaction if a transaction does a lot of computation the 
// gas cost will be higher and if the transaction does less computation the gas will be less when you send the transaction you specify  
// what is maximum amount of gas that you are willing to spend to execute this transaction by default remix puts a very  
//In debug of remix you can see cost of transaction and execution (of gas)
//Be careful with the gas limit could be waste of money, first is better to analyze in sendbox how much is it then is possible to send in real blockchain
