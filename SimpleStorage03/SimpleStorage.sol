//Create simple storage smart contract that is able to store modify and read string value 
//variable that will be accesible from outside is nessecary to use "public" after that we can definate the name of variable this example called "data"
//after name of variable we can use location like storage memory which means that it will be actually saved on the blockchain so it's not temporary variable it's something that you will be able to read in other smart contract execution in the future when you declare
//when we declare variable it's possible to give it an initial value like data = "mydata";
// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;


//The contract to call definied variable with value 
// contract SimpleStorage {
//     string public data = "mydata";    
//     }

//Function to change the value of the data variable anytime (this function added button for edit/set value)
contract SimpleStorage {
    string public data;    

    function set(string memory _data) public {
        //assing undescore data to data (variable) and now its access to storage
        data = _data;
        }
    
//the log/debug is in Json 

//Function for getter data variable
//view is a is a similer like a pure but different is that with pure you can read a static value that you hard-code in a function, but if you want to read the storage o the smart contract then you need the view 

function get() view public returns (string memory) {
        return data;
    }

}

//Environment during deploy, there is a possible to connect to several blockchain, before deploy there are the options to choose "type" of blockchain, e.g Javascript VM is blochain in sandbox of browser, this blockchain never will be save. 
//More info about environment is in documentation: https://remix-ide.readthedocs.io/en/latest/run.html
//In sandbox we have 15 tests adress where we can send a contract 
