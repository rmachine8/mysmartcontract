// SPDX-License-Identifier: MIT
//It's nesecery from version solidity 0.6.8 to use SPDX license: https://forum.openzeppelin.com/t/solidity-0-6-8-introduces-spdx-license-identifiers/2859
//Version of solidity for compiler is like "pragma solidity >=0.7.0 <0.9.0;"
//possible to use compiler if is it version between 0.7 - 0.9
pragma solidity >=0.7.0 <0.9.0;


//definition of contract
//between brackets we can put definiton of action
contract MyContract {}

//WE can have several definition of contracts MyContract1,MyContract2  and so on...


//Create simple storage smart contract that is able to store modify and read string value 
//variable that will be accesible from outside is nessecary to use "public" after that we can definate the name of variable this example called "data"
//after name of variable we can use location like storage memory which means that it will be actually saved on the blockchain so it's not temporary variable it's something that you will be able to read in other smart contract execution in the future when you declare
//when we declare variable it's possible to give it an initial value like data = "mydata";
// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;


//The contract to call definied variable with value 
// contract SimpleStorage {
//     string public data = "mydata";    
//     }

//Function to change the value of the data variable anytime (this function added button for edit/set value)
contract SimpleStorage {
    string public data;    

    function set(string memory _data) public {
        //assing undescore data to data (variable) and now its access to storage
        data = _data;
        }
    
//the log/debug is in Json 

//Function for getter data variable
//view is a is a similer like a pure but different is that with pure you can read a static value that you hard-code in a function, but if you want to read the storage o the smart contract then you need the view 

function get() view public returns (string memory) {
        return data;
    }

}

//Environment during deploy, there is a possible to connect to several blockchain, before deploy there are the options to choose "type" of blockchain, e.g Javascript VM is blochain in sandbox of browser, this blockchain never will be save. 
//More info about environment is in documentation: https://remix-ide.readthedocs.io/en/latest/run.html
//In sandbox we have 15 tests adress where we can send a contract 

// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract HelloWorld {

// functions are similar like in JS
// It's necessary to use "public" word key for function for call from outside the smart contract 
// In solidity is needs to specify what is return type of a function in this example it's string (Solidity = type language),because we can save variables to the blockchaing we need to specify so we need to say solidity that is temporary with the key word "memory" 
// Be careful to use in definition of function because it's with "S" but in function like when something returns in function it's withou "S"    
// In solidity function could be modify something on the blockchain or just return (read something). If we dont want to change something on blockchain we need to use work key "pure" it says this is a read-only function
    function Hello() pure public returns(string memory) {

        return "Hello World!!!!";
        }

    }


    // SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;
//Create contract with array
contract AdvancedStorage {
    //integer with unit type
    uint[] public ids;

    //Add new value to the array (careful, started from 0 like in JS)
    function add (uint id) public {
        ids.push(id);
    }
    //Get exactly possition of array 
    function get (uint position) view public returns (uint){
        return ids[position];
    }
    //To list all values of array
    function getAll() view public returns (uint [] memory) {
        return ids;
    }
    //Function to get lenght of array 
    function lenght() view public returns (uint) {
        return ids.length;
    }
}


// Gas limit - when send transaction to the etherium network you have to pay "gas". 
// Gas is an abstract unit that measure the computational diffulty of executing the transaction if a transaction does a lot of computation the 
// gas cost will be higher and if the transaction does less computation the gas will be less when you send the transaction you specify  
// what is maximum amount of gas that you are willing to spend to execute this transaction by default remix puts a very  
//In debug of remix you can see cost of transaction and execution (of gas)
//Be careful with the gas limit could be waste of money, first is better to analyze in sendbox how much is it then is possible to send in real blockchain
