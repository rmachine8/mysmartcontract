// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

//Crude means create, update, read and delete

contract Crud {
    struct User {
        uint id;
        string name;

    }
    User [] public users;
    uint public nextId = 1;

//Function to create
    function create (string memory name) public {
        users.push(User(nextId, name));
        nextId++;
    }
//Function to read
    function read (uint id) view public returns (uint, string memory) {
        uint i = find(id);
                return(users[i].id, users[i].name);
    }
//Function to update
    function update(uint id, string memory name) public {
        uint i = find(id);  
        users[i].name = name; 
    }
//Function to delete
    function destroy(uint id)public {
        uint i = find(id);
        delete users [i];
    }

    function find (uint id) view internal returns(uint) {
        for(uint i = 0; i < users.length; i++) {
            if (users[i].id == id){
                return i;
            }
        }
        revert("User does not exist!!!!");
    }
}