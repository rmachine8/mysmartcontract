// SPDX-License-Identifier: MIT
//It's nesecery from version solidity 0.6.8 to use SPDX license: https://forum.openzeppelin.com/t/solidity-0-6-8-introduces-spdx-license-identifiers/2859
//Version of solidity for compiler is like "pragma solidity >=0.7.0 <0.9.0;"
//possible to use compiler if is it version between 0.7 - 0.9
pragma solidity >=0.7.0 <0.9.0;


//definition of contract
//between brackets we can put definiton of action
contract MyContract {}

//WE can have several definition of contracts MyContract1,MyContract2  and so on...

