// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract HelloWorld {

// functions are similar like in JS
// It's necessary to use "public" word key for function for call from outside the smart contract 
// In solidity is needs to specify what is return type of a function in this example it's string (Solidity = type language),because we can save variables to the blockchaing we need to specify so we need to say solidity that is temporary with the key word "memory" 
// Be careful to use in definition of function because it's with "S" but in function like when something returns in function it's withou "S"    
// In solidity function could be modify something on the blockchain or just return (read something). If we dont want to change something on blockchain we need to use work key "pure" it says this is a read-only function
    function Hello() pure public returns(string memory) {

        return "Hello World!!!!";
        }

    }